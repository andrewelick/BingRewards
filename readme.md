# Bing Rewards bot

This is a simple bot to help you earn bing rewards points without breaking a sweat!


## Requirements

* Microsoft account
* `Node.js` and `npm` installed on your machine

## Setup

* Clone this repo to your local machine
* In a terminal, navigate to the root directory of this project and run `npm install` to install the required dependencies.
* Create a `.env` file with the contents of the `env.sample` file included. Be sure to enter your email and password for the Bing credentials.

## Begin

To start the script simply run `node BingSearch.js` and the script will handle the rest!