require('dotenv').config(); // Environment variables
const puppeteer = require('puppeteer'); // browser automation

// Random number to generate time waits
const randomNumber = (min, max) => {
    return Math.random() * (max - min) + min;
};

// Delay
const delay = (time) => {
    return new Promise((resolve) => setTimeout(resolve, time));
};

// Generate random search value
const generateRandomString = () => {
    const wordsList = require('./words.json');
    const amountWords = 4;
    const sentence = [];

    // Grab a random word from words list and add to sentence
    for (let counter = 0; counter < amountWords; counter++) {
        const randomIndex = Math.floor(Math.random() * wordsList.length);
        sentence.push(wordsList[randomIndex]);
    };

    return sentence.join(' ');
};

// Login to bing account and start making searches
const searchBing = async () => {

    console.log('Launching browser...');

    // Launch browser and create page
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    console.log('Browser launched!');
    console.log('Navigating to login page...');

    // Navigate to bing login page
    const url = 'https://login.live.com/login.srf?wa=wsignin1.0&rpsnv=13&ct=1612291714&rver=6.7.6631.0&wp=MBI_SSL&wreply=https%3a%2f%2fwww.bing.com%2fsecure%2fPassport.aspx%3frequrl%3dhttps%253a%252f%252fwww.bing.com%252f%253fwlexpsignin%253d1%26sig%3d0D2A7C3AF6906CAF2B7873E8F7986D5E&lc=1033&id=264960&CSRFToken=98333ad0-d09e-4fd8-89d1-2e8e5f26d3ff&aadredir=1';
    await page.goto(url);

    console.log('Navigated to login page!');
    console.log('Waiting for delay...');

    await delay(randomNumber(1500, 3500));

    console.log('Inputting login email...');

    // Input login email
    await page.type('input[type=email]', process.env.BING_EMAIL, { delay: 111 });
    await page.click('input[type=submit]', { delay: 87 });

    console.log('Done inputting login email!');
    console.log('Waiting for delay...');
    
    await delay(randomNumber(1500, 3500));

    console.log('Inputting login password...');

    // Input login password
    await page.type('input[type=password]', process.env.BING_PASSWORD, { delay: 241 });
    await page.click('input[type=submit]', { delay: 73 });

    console.log('Done inputting login password!');

    // Search multiple times until we hit daily limit
    for (let counter = 1; counter <= parseInt(process.env.SEARCH_AMOUNT); counter++) {

        console.log(`Beginning search #${counter}`);
        console.log('Waiting for delay...');

        // Wait until page load and random searches
        await delay(randomNumber(3500, 5500));

        console.log('Clearing previous search input...');

        // Clear previous input text and wait
        await page.evaluate(() => document.getElementById('sb_form_q').value = '');
        await delay(randomNumber(1500, 3000));

        console.log('Done clearing search input!');
        console.log('Creating new search...');

        // Generate random sentence, type in, and search
        const randomString = generateRandomString();
        await page.type('#sb_form_q', randomString, { delay: randomNumber(100, 200) });
        await (await page.$('input[type=search]')).press('Enter');

        console.log('Done creating new search!');
        console.log('Waiting for delay...');

        // Wait until page has fully loaded
        await delay(randomNumber(5000, 8000));

        console.log(`Done with search #${counter}`);
        console.log('');
    };

    console.log('Completed all searches!');
    console.log('Closing browser...')

    // Close browser
    await browser.close();

    console.log('Done!');
};

// Begin the automation function
searchBing();